from auxiliaryfunctions import get_net_from_dtnsim_CP,highlight_routing_decisions,net_to_dot,gen_routing_for_DTNSim
from newmodel import get_best_routing
import os
from copy import deepcopy
import matplotlib.pyplot as plt

PATH = "/home/nando/Desktop/ICCAnalysisNet/contactPlan"
for cp in [7]:
    net = get_net_from_dtnsim_CP(PATH + "/0.2_%d"%cp,10)

    net_numer_path = PATH + "/net%d"%cp
    os.system('mkdir ' + net_numer_path)

    NUM_OF_NODES = net["NUM_OF_NODES"]
    f_expected_del_ratio = []
    for pf in [0.,0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]:
        pf_path = net_numer_path + "/pf=%1.1f" % pf
        os.system('mkdir ' + pf_path)

        # Set Contact Failure Probability
        for c in net['CONTACTS']:
            c['pf'] = pf

        # Add point to function
        pf_expected_del_ratio = 0.

        # Generate best routing for a all to all traffic pattern
        for n1 in range(NUM_OF_NODES):
            for n2 in range(NUM_OF_NODES):
                if n1 == n2: continue  # It is not allowed self traffic
                print("[Debug] BRUF from %d to %d" % (n1, n2))
                traffic_path = pf_path + "/%d-%d" % (n1, n2)
                os.system('mkdir ' + traffic_path)
                best_routing = get_best_routing(net, traffic_path, traffic={'from': n1, 'to': n2, 'ts': 0})

                pf_expected_del_ratio += best_routing[0]  # Add expected delivery ratio for traffic n1 to n2
                if best_routing[1] != {}:
                    net_copy = deepcopy(net)
                    highlight_routing_decisions(net_copy, best_routing[1])
                    net_to_dot(net_copy, traffic_path)

                    f = open(pf_path + '/routing-dtnsim.txt', 'a')
                    f.write("# From %d to %d\n" % (n1, n2))
                    for c in gen_routing_for_DTNSim(net, best_routing[1],n1, n2, 10):
                        f.write("%d %d %d %d %d \n" % (c["c_from"] + 1, c["source"] + 1,c["target"] + 1, c["expire_time"], c["contact_id"] + 1))
                    f.write("\n\n")
                    f.close()
                else:
                    net_to_dot(net, traffic_path)
                    print("[Debug] There is not route from %d to %d" % (n1, n2))

        # Compute average for probability of failure pf
        f_expected_del_ratio.append((pf, pf_expected_del_ratio / (NUM_OF_NODES * (NUM_OF_NODES - 1))))

    f_expected_del_ratio.append((1.0,0))
    f = open(net_numer_path + '/expected-del-ratio.txt', 'w')
    f.write(str(f_expected_del_ratio))
    f.close()
    # plot 10Net
    line_up, = plt.plot([x[0] for x in f_expected_del_ratio], [y[1] for y in f_expected_del_ratio], '--o',
                        label="E(Del_Ratio) BestRouting")
    plt.legend(handles=[line_up])
    plt.xlabel("Link Failure Probability")
    plt.ylabel("Expected Delivery Ratio")
    plt.grid(color='gray', linestyle='dashed')
    plt.savefig(net_numer_path + '/expected-del-ratio.png')
    plt.clf()
    plt.cla()
    plt.close()