PRISM
=====

Version: 4.4
Date: Tue Aug 14 11:57:49 ART 2018
Hostname: Nando
Memory limits: cudd=1g, java(heap)=910.5m
Command line: prism /home/nando/Desktop/BRUF-STINT18/examples/Case2//model.prism -pctl 'Pmax=?[F node=3]' -exportadv /home/nando/Desktop/BRUF-STINT18/examples/Case2//adv.tra -exportlabels /home/nando/Desktop/BRUF-STINT18/examples/Case2//label.txt -exportstates /home/nando/Desktop/BRUF-STINT18/examples/Case2//states.txt -s

Parsing model file "/home/nando/Desktop/BRUF-STINT18/examples/Case2//model.prism"...

1 property:
(1) Pmax=? [ F node=3 ]

Type:        MDP
Modules:     bundle 
Variables:   node ts n0 n1 n2 n3 

Building model...

Computing reachable states...

Reachability (BFS): 8 iterations in 0.00 seconds (average 0.000125, setup 0.00)

Time for model construction: 0.029 seconds.

Warning: Deadlocks detected and fixed in 4 states

Type:        MDP
States:      23 (1 initial)
Transitions: 33
Choices:     28

Transition matrix: 197 nodes (3 terminal), 33 minterms, vars: 9r/9c/6nd

Exporting list of reachable states in plain text format to file "/home/nando/Desktop/BRUF-STINT18/examples/Case2/states.txt"...

Exporting labels and satisfying states in plain text format to file "/home/nando/Desktop/BRUF-STINT18/examples/Case2/label.txt"...

Warning: Disabling Prob1 since this is needed for adversary generation

---------------------------------------------------------------------

Model checking: Pmax=? [ F node=3 ]

Warning: Disabling Prob1 since this is needed for adversary generation

Prob0A: 5 iterations in 0.00 seconds (average 0.000200, setup 0.00)

yes = 6, no = 8, maybe = 9

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=23, nc=14, nnz=19, k=2] [0.3 KB]
Building action information... [0.1 KB]
Creating vector for yes... [0.2 KB]
Allocating iteration vectors... [2 x 0.2 KB]
Allocating adversary vector... [0.1 KB]
TOTAL: [1.0 KB]

Starting iterations...

Iterative method: 7 iterations in 0.00 seconds (average 0.000000, setup 0.00)

Adversary written to file "/home/nando/Desktop/BRUF-STINT18/examples/Case2//adv.tra".

Value in the initial state: 0.5

Time for model checking: 0.006 seconds.

Result: 0.5 (value in the initial state)

---------------------------------------------------------------------

Note: There were 3 warnings during computation.

