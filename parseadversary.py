from functools import reduce

def parse_state(line):
    state_number = int(line[:line.index(':')])
    state_atributes = line[line.index('(') + 1: -1].split(',')
    node = int(state_atributes[0])
    ts = int(state_atributes[1])
    #We ignore visited nodes variables
    return (state_number, node, ts)

def parse_states(path_to_states):
    f = open(path_to_states,'r')
    states = [parse_state(line) for line in f.readlines()[1:]]
    #check states ordering
    if not reduce(lambda x,y: x and y, [states[i][0] == i for i in range(len(states))]):
        print("[Error] parse_states: state are not ordered as it is expected.")
        exit()
    return states

def parse_decision(d):
    aux = d.split(" ")
    return {'from': int(aux[0]), 'to': int(aux[1]), 'action': aux[3][:-1], 'visited':False,'childs':[]}

def parse_adversary_file(path_to_adv, path_to_label):
    adv = open(path_to_adv, 'r')
    label = open(path_to_label, 'r')
    adversary = list(map(lambda l: parse_decision(l), [l for l in adv][1:]))

    #Get initial state identifier
    initial_state = -1
    for line in label.readlines():
        line = line.split(" ")
        if len(line)>1 and line[1] == '0\n':
            initial_state = int(line[0][:-1])
            break

    if initial_state != -1:
        #print("[Debug] parse_adversary_file: Initial state is %d" % initial_state)
        root = {'from': initial_state, 'to': initial_state, 'childs': [], 'action': "next"}
    else:
        print("[Error] parse_adversary_file: There is not initial state in %s"%path_to_label)
        exit()

    q = [root]
    while len(q) > 0:
        current = q.pop(0)
        for d in adversary:
            if d["from"] == current["to"]:
                #If current state was not already added to queue, add it. On the other case only append as a child
                if not d['visited']:
                    d['visited'] = True
                    q.append(d)
                current["childs"].append(d)
    return root



def check_initial(state, traffic):
    return state[1] == traffic['from'] and state[2] == 0

def check_next_transition(sfrom, sto):
    return sfrom[1] == sto[1] and sfrom[1] + 1 == sto[1]

def is_send_success(sfrom, sto, action):
    action = action[5:]
    #send_from = int(action[0:action.index('_')])
    send_to =  int(action[action.index('_')+2:action.rindex('_')])
    #send_ts = int(action[action.rindex('_')+2:])

    if sto[1] == send_to:
        return True
    else:
        return False




def dfs(root,states):
    action = root['action']
    ##########################################
    #BIG CASE ACTION IS 'next'
    #########################################
    if action == 'next':
        #Base case: Non childs
        if len(root['childs']) == 0:
            return {}

        #Case 1 child: It can be next or send:
        elif len(root['childs']) == 1:
            c = root['childs'][0]
            if 'send' in c['action']:
                #Child is send: Send action are uploaded, and then continue exploring next routing decisions
                new_root = {'action':c['action']}
                c = dfs(c,states) #Explore child, it must be empty, fail or send.
                if c != {}:
                    # If it a send follows by non-empty dic then it must be "send fail" or "send success"
                    if 'fail' not in c.keys() and 'send' not in c.keys():
                        # Otherwise report the error
                        print(
                            "[Error] dfs: Next transition follows by send state but it is not follow by a send-fail or send-success")
                        print(root)
                        exit()

                    new_root.update(c)

                #All was good
                return new_root
            else:
                # Child is next: Continue exploring routing decisions
                return dfs(c, states)

        # Case 2 child send
        elif len(root['childs']) == 2:
            c1 = root['childs'][0]
            c2 = root['childs'][1]

            # Check: If it has to child, both must have the same send action
            if not ('send' in c1['action'] and c1['action'] == c2['action']):
                print("[Error] dfs: Next transition follows by two states but they are not send-fail, send-success")
                print(root)
                exit()

            # Send action are uploaded, and then continue exploring next routing decisions
            new_root = {'action': c1['action']}
            c1 = dfs(c1, states)  # Explore child, it must be empty, fail or send.
            c2 = dfs(c2, states)  # Explore child, it must be empty, fail or send.
            if c1 != {}:
                # Check
                if 'fail' not in c1.keys() and 'send' not in c1.keys():
                    print(
                        "[Error] dfs: Next transition follows by two states but they are not send-fail or send-success")
                    print(root)
                    exit()
                new_root.update(c1)
            if c2 != {}:
                # Check
                if ('fail' not in c2.keys() and 'send' not in c2.keys()) or c1.keys() == c2.keys():
                    print(
                        "[Error] dfs: Next transition follows by two states but they are not send-fail or send-success")
                    print(root)
                    exit()
                new_root.update(c2)

            # All was good
            return new_root
        else:
            # Error
            print("[Error] dfs: Next transition follows by %d states!" % len(root['childs']))
            print(root)
            exit()


    ##########################################
    #BIG CASE ACTION IS 'next'
    #########################################
    elif 'send' in action:

        # It must be a success send or fail send, it must be follow by next or another send
        # If it is follow by other send, it must be uploaded
        sfrom = states[root['from']]
        sto = states[root['to']]
        # IF it is a success send, We will return {'send':} (When child won't empty)
        # If it is a fail send: We will return {'fail':} (When child won't empty)
        key = "send" if is_send_success(sfrom, sto, action) else "fail"

        # Base case
        if len(root['childs']) == 0:
            # Notice action are always uploaded on the before call
            return {}

        elif len(root['childs']) == 1:
            c = root['childs'][0]

            if 'send' in c['action']:
                # Upload action
                new_child = {'action': c['action']}
                c = dfs(c, states)  # Explore child, it must be empty, fail or send.
                if c != {}:
                    # If it a send follows by non-empty dict then it must be "send fail" or "send success"
                    if 'fail' not in c.keys() and 'send' not in c.keys():
                        print(
                            "[Error] dfs: Send transition follows by send state but it is not follow by a send-fail or send-success")
                        print(root)
                        exit()

                    #The decisions if after current child send action
                    new_child.update(c)

                #All goes well
                return {key:new_child}
            else:
                # Continue exploring routing decisions
                child = dfs(c, states)
                if child != {}:
                    return {key: child}  # If child is not empy reutrn as {fail:child} or {send:child}
                else:
                    return {}  # Notice action are always uploaded on the before call

        # case 2 child send
        elif len(root['childs']) == 2:
            c1 = root['childs'][0]
            c2 = root['childs'][1]

            # Check: If it has to child, both must have the same send action
            if not ('send' in c1['action'] and c1['action'] == c2['action']):
                print("[Error] dfs: Send transition follows by two states but they are not send-fail, send-success")
                print(root)
                exit()

            new_child = {'action': c1['action']}
            c1 = dfs(c1, states)  # Explore child, it must be empty, fail or send.
            c2 = dfs(c2, states)  # Explore child, it must be empty, fail or send.
            if c1 != {}:
                # Check
                if 'fail' not in c1.keys() and 'send' not in c1.keys():
                    print("[Error] dfs: Send transition follows by two states but they are not send-fail or send-success")
                    print(root)
                    exit()
                new_child.update(c1)
            if c2 != {}:
                # Check
                if ('fail' not in c2.keys() and 'send' not in c2.keys()) or c1.keys() == c2.keys():
                    print("[Error] dfs: Send transition follows by two states but they are not send-fail or send-success")
                    print(root)
                    exit()
                new_child.update(c2)

            # All was good
            return {key: new_child}  # return as {fail:child} or {send:child}

    else:
        print("[Error] dfs: Action is not next nor send")
        print(root)
        exit()


def get_routing_decisions(path_to_adversary, path_to_state, path_to_label):
    adversary = parse_adversary_file(path_to_adversary, path_to_label)
    states = parse_states(path_to_state)

    return dfs(adversary,states)
